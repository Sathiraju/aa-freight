��    H      \  a   �         g   !  8   �     �     �     �     �     �                    &  
   7  J   B     �     �     �  �   �     r  	   �     �     �  	   �     �     �  V   �  &   	  
   E	  
   P	     [	     a	  	   e	     o	     {	     �	     �	     �	     �	     �	  S   �	     )
     <
     C
     b
     {
     �
     �
     �
     �
     �
  ,   �
  5   �
  <   !  3   ^  :   �     �  &   �  !   �  G     �   g     @     D     V  %   ]  "   �     �     �     �  n   �     -     ;     @  �  C  �   0  N   �       2   )     \  2   p     �     �     �  "   �     �       �   (  >   �     �     �  2       :     U     h     }     �  %   �     �  �   �  >   ^  $   �     �     �     �     �     �          %     8      K     l  +   s  �   �  !   �     �  B   �  -   �  %   $  $   J     o     ~     �     �  X   �  a     s   i  a   �  s   ?     �  A   �  K     v   X  �  �  	   g  #   q  
   �  E   �  A   �     (  
   <     G  �   S     J     f     o     D       "       2                 1          6       7   @      9   0      #             =         A   )   -   $      ;       +   H   8         /                            
   '                	          C   4      (         %   :   .   3         &   B      *   5           F   ,          G   !                     ?               >   <               E    
            Statistics calculated for all contracts finished withn the last %(max_days)s days
         Accessing structures with token from %(token_char_name)s Active Add / Update Location Added: Additional Instructions: All Alliance Availability: Base price: Both directions: Calculator Can not setup contract handler, because %s is not a member of any alliance Click To Calculate Reward! Collateral: Collaterals Contract Handler setup started for %(organization)s organization with %(character)s as sync character. Operation mode: %(operation_mode)s. Started syncing of courier contracts.  Contract Type: Contracts Corporation Courier Customers Days to complete: Expiration: Failed to add location with token from %(character)s for location ID %(id)s: %(error)s Fetch contracts from Eve Online server Fix price: From / To: From: ISK Location: Max Volume: Max. Collateral: Min Volume: Min. Collateral: Minimum price: Name No price calculated yet Note that this route is bidirectional, so Location and Destination can be switched. Pilot Corporations Pilots Please define a pricing/route! Price add-on collateral: Price add-on per m3: Pricing details Reward Reward: Rewards Routes Send notifications for outstanding contracts Sent customer notification for contract %d to Discord Sent customer notification for selected contracts to Discord Sent pilots notification for contract %d to Discord Sent pilots notification for selected contracts to Discord Ship To: Started sending notifications for: %s  Started syncing contracts for: %s Started updating %d locations. This can take a short while to complete. There already is a contract handler installed for a different operation mode. You need to first delete the existing contract handler in the admin section before you can set up this app for a different operation mode. To: Toggle navigation Totals Update pricing info for all contracts Update selected locations from ESI Updated: Volume Volume: You can only use your main or alt characters to setup the contract handler. However, character %s is neither.  Your contract days m3 Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-13 14:51+0000
Last-Translator: - -, 2023
Language-Team: Russian (https://app.transifex.com/kalkoken-apps/teams/107978/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 
Статистика подсчитана для всех контрактов, завершенных за последние %(max_days)s дней
     Получение структур по токену от %(token_char_name)s Активно Добавить / обновить локацию Добавлено: Дополнительные инструкции: Все Альянс Доступность: Базовая стоимость: Оба направления: Калькулятор Невозможно добавить обработчика контрактов потому, что %s не член альянса Нажмите чтобы подсчитать награду! Залог: Залоги Обработчик контрактов добавлен синхронизирующим персонажем %(character)s для организации %(organization)s. Режим работы: %(operation_mode)s. Началась синхронизация курьерских контрактов. Тип контракта: Контракты Корпорация Курьер Клиенты Дней для завершения: Срок действия: Не удалось добавить локацию с токеном от %(character)s для ID локации %(id)s: %(error)s Получаю контракты с сервера Eve Online Фиксированная цена: От / Кому: От: ISK Местоположение: Макс объём: Макс залог: Мин объём: Мин залог: Минимальная цена: Имя Цена пока не подсчитана Обратите внимание, что этот маршрут является двунаправленным, поэтому местоположение и пункт назначения можно переключать. Корпорация пилота Пилоты Пожалуйста определите цену/маршрут! Надбавка к цене за залог: Надбавка к цене за m3: Подробности о ценах Награда Награда: Награды Маршруты Отправить оповещения о просроченных контрактах Отправить клиенту оповещение о контракте %d в Дискорд Отправить клиенту оповещение о выбранных контрактах в Дискорд Отправить пилотам оповещения о контракте %d в Дискорд Отправить пилотам оповещения о выбранных контрактах в Дискорд Доставить в: Началась отправка оповещений для: %s Началась синхронизация контрактов для: %s Началось обновление %d локаций. Это может занять некоторое время. Для другого режима работы уже установлен обработчик контрактов. Вам необходимо сначала удалить существующий обработчик контрактов в админ-панели, прежде чем вы сможете настроить это приложение для другого режима работы. Кому: Включить навигацию Всего Обновление стоимости всех контрактов Обновить выбранные локации через ESI Обновлено: Объём Объём: Вы можете использовать вашего основного или альт персонажа для добавления обработчика контрактов. Однако, персонаж %s ни то, ни другое. Ваши контракты дней m3 